#define _GNU_SOURCE

#include <mpi.h>
#include <sched.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int p2p(int argc, char **argv) {

	int rang, nbprocs, dest = 0, source, etiquette = 50;
	MPI_Status statut;
	char message[100];

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rang);
	MPI_Comm_size(MPI_COMM_WORLD, &nbprocs);

	if (rang == nbprocs - 1) {
		sprintf(message, "Bonjour de la part de P%d", rang);
		MPI_Send(message, strlen(message) + 1, MPI_CHAR, rang - 1, etiquette, MPI_COMM_WORLD);
	} else if (rang != 0) {
		sprintf(message, "Bonjour de la part de P%d", rang);
		MPI_Recv(message, 100, MPI_CHAR, rang + 1, etiquette, MPI_COMM_WORLD, &statut);
		sprintf(message, "%s, %d", message, rang);
		MPI_Send(message, strlen(message) + 1, MPI_CHAR, rang - 1, etiquette, MPI_COMM_WORLD);
	} else {
		MPI_Recv(message, 100, MPI_CHAR, 1, etiquette, MPI_COMM_WORLD, &statut);
		printf("%s !\n", message);
	}
	MPI_Finalize();
	return EXIT_SUCCESS;
}

int test(int argc, char **argv) {

	int rang = -1;
	int nbprocs = 0;
	int namelen;
	int cpu_id;
	char processor_name[100];

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rang);
	MPI_Comm_size(MPI_COMM_WORLD, &nbprocs);
	MPI_Get_processor_name(processor_name, &namelen);

	cpu_id = sched_getcpu();

	printf("Hello from process %d of %d\n", rang, nbprocs);
	printf("Node : %s\n", processor_name);
	printf("CPU ID : %d\n", cpu_id);

	MPI_Finalize();

	return EXIT_SUCCESS;
}

int main(int argc, char **argv) {
	// test(argc, argv);
	p2p(argc, argv);
	return 0;
}
