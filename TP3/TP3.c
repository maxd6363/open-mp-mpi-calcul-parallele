#define _GNU_SOURCE

#include <mpi.h>
#include <sched.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 16
#define NB_PROC 8

typedef enum { false,
			   true } bool;

int rang;

int A[] = {3, 6, 6, 8, 0, 1, 5, 2, 9, 9, 5, 0, 7, 2, 6, 9,
		   3, 2, 4, 0, 5, 3, 9, 8, 4, 1, 4, 5, 3, 2, 2, 6,
		   6, 6, 4, 6, 9, 1, 0, 2, 7, 6, 8, 1, 5, 6, 6, 3,
		   8, 5, 7, 8, 1, 8, 9, 4, 7, 4, 9, 0, 5, 2, 7, 9,
		   7, 6, 7, 6, 0, 5, 8, 1, 1, 6, 4, 3, 2, 7, 3, 9,
		   5, 4, 4, 7, 7, 9, 5, 6, 5, 6, 8, 7, 5, 0, 2, 5,
		   1, 8, 4, 7, 4, 5, 4, 7, 7, 6, 8, 0, 3, 4, 8, 8,
		   7, 4, 8, 3, 4, 9, 5, 6, 8, 2, 0, 6, 5, 6, 8, 9,
		   1, 4, 2, 0, 8, 2, 0, 5, 5, 1, 2, 4, 0, 8, 3, 7,
		   7, 6, 7, 4, 8, 5, 4, 3, 3, 4, 4, 0, 3, 5, 4, 1,
		   3, 4, 2, 0, 3, 6, 9, 7, 8, 3, 5, 0, 2, 1, 4, 0,
		   6, 2, 9, 0, 2, 3, 9, 8, 2, 5, 2, 5, 6, 9, 5, 2,
		   8, 5, 0, 9, 9, 5, 9, 5, 1, 9, 4, 5, 7, 4, 4, 0,
		   9, 0, 4, 8, 0, 7, 5, 0, 1, 5, 5, 6, 8, 6, 0, 6,
		   9, 3, 2, 4, 1, 3, 1, 7, 1, 9, 1, 0, 9, 2, 7, 1,
		   0, 5, 8, 0, 6, 1, 7, 9, 0, 9, 8, 2, 2, 4, 6, 7};

int X[] = {6, 6, 3, 3, 7, 7, 3, 9, 9, 6, 3, 4, 2, 6, 6, 5};

bool isRoot() {
	return rang == 0;
}

int multiply(int argc, char **argv) {
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rang);

	int lineA[N * N / NB_PROC];
	int result[N / NB_PROC];
	int resultChildren[N];

	MPI_Bcast(X, N, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Scatter(A, N * N / NB_PROC, MPI_INT, lineA, N * N / NB_PROC, MPI_INT, 0, MPI_COMM_WORLD);

	for (int i = 0; i < N / NB_PROC; i++) {
		result[i] = 0;
		for (int j = 0; j < N; j++)
			result[i] += lineA[i * N + j] * X[j];
	}

	MPI_Gather(result, N / NB_PROC, MPI_INT, resultChildren, N / NB_PROC, MPI_INT, 0, MPI_COMM_WORLD);

	if (isRoot())
		for (int i = 0; i < N; i++)
			printf("%d\n", resultChildren[i]);
	MPI_Finalize();
	return EXIT_SUCCESS;
}

int main(int argc, char **argv) {
	multiply(argc, argv);
	return 0;
}
