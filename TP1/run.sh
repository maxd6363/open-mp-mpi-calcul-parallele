#!/bin/bash

make
{ /usr/bin/time --format="%E\n%U" ./out $1 ; } 2> time.dat

cat time.dat | cut -d : -f 2

rm time.dat