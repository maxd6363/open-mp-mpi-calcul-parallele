#define _GNU_SOURCE
#include <omp.h>
#include <sched.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

typedef enum { false,
			   true } bool;

void test() {
	int tid = -1;
	char hostname[1024];
	gethostname(hostname, 1024);
	printf("Before PARALLEL REGION TID %d: There are %d threads on CPU %d of %s\n\n",
		   omp_get_thread_num(), omp_get_num_threads(), sched_getcpu(), hostname);

// omp_set_num_threads(4);
// #pragma omp parallel firstprivate(tid)
#pragma omp parallel num_threads(16) firstprivate(tid)
	{
		tid = omp_get_thread_num();
		if (!tid)
			printf("In the PARALLEL REGION TID %d: There are %d threads in process\n",
				   omp_get_thread_num(), omp_get_num_threads());
		printf("Hello World from TID %d / %d on CPU %d of %s!\n\n",
			   tid, omp_get_num_threads(), sched_getcpu(), hostname);
	}

	printf("After PARALLEL REGION TID %d: There are %d threads\n\n",
		   tid, omp_get_num_threads());
}

int **new(int size) {
	int **A = malloc(size * sizeof(int *));
	for (size_t i = 0; i < size; i++)
		A[i] = malloc(size * sizeof(int));
	return A;
}

void delete(int **matrix, int size) {
	for (size_t i = 0; i < size; i++)
		free(matrix[i]);
	free(matrix);
}

void fill(int **matrix, int size) {
	for (size_t i = 0; i < size; i++)
		for (size_t j = 0; j < size; j++)
			matrix[i][j] = rand() % 10;
}

void empty(int **matrix, int size) {
	for (size_t i = 0; i < size; i++)
		for (size_t j = 0; j < size; j++)
			matrix[i][j] = 0;
}

void print(int **matrix, int size) {
	for (size_t i = 0; i < size; i++) {
		for (size_t j = 0; j < size; j++)
			printf("%d ", matrix[i][j]);
		printf("\n");
	}
	printf("\n");
}

bool checkMultiply(int **A, int **B, int **C, int size) {
	int **D = new (size);

	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			D[i][j] = 0;
			for (int k = 0; k < size; k++) {
				D[i][j] += A[i][k] * B[k][j];
			}
		}
	}

	for (int i = 0; i < size; i++)
		for (int j = 0; j < size; j++)
			if (C[i][j] != D[i][j])
				return false;

	delete (D, size);
	return true;
}

void matrice(int size) {
	int i, j, k;
	int **A = new (size);
	int **B = new (size);
	int **C = new (size);

	fill(A, size);
	fill(B, size);
	empty(C, size);

	#pragma omp parallel for private(i, j, k) shared(A, B, C)
	for (i = 0; i < size; i++)
		for (j = 0; j < size; j++)
			for (k = 0; k < size; k++)
				C[i][j] += A[i][k] * B[k][j];

	// Used to check if parallel calculus is correct
	// printf("%s\n", checkMultiply(A, B, C, size) == true ? "true" : "false");

	delete (A, size);
	delete (B, size);
	delete (C, size);
}

int main(int argc, char const *argv[]) {
	if (argc != 2) {
		printf("Wrong number of args : %d\n", argc - 1);
		return EXIT_FAILURE;
	}
	matrice(atoi(argv[1]));
	return EXIT_SUCCESS;
}
