#define _GNU_SOURCE
#include <float.h>
#include <math.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

typedef enum { false,
			   true } bool;

bool equals(double d1, double d2) {
	return fabs(d1 - d2) < DBL_EPSILON;
}

bool matrixEquals(double *m1, double *m2, int size) {
	for (int i = 0; i < size * size; i++) {
		if (!equals(m1[i], m2[i]))
			return false;
	}
	return true;
}

double rand01(void) {
	return (double)rand() / ((double)RAND_MAX);
}

int main(int argc, char **argv) {
	// 20000, 0.00001, 100000 : NOT PARALLELE -> 2.25 s
	// 20000, 0.00001, 100000 : PARALLELE 16 THREADS -> 0.27 s
	  
	int i, j, k, n, max_iter;
	double start, finish, elapsed;
	double tol;
	double beta, alpha;
	double rr, rr0, rr1, pq;

	if (argc != 4) {
		n = 20000;
		tol = 0.00001;
		max_iter = 100000;
		printf("Usage : [n] [tol] [max_iter]\n");
		printf("Using n=%d tol=%lf max_iter=%d\n", n, tol, max_iter);
	} else {
		n = atoi(argv[1]);
		tol = atof(argv[2]);
		max_iter = atoi(argv[3]);
	}

	int numberOfThreads = 16;
	omp_set_num_threads(numberOfThreads);
	printf("Setting up %d threads\n", numberOfThreads);

	//--------  Données matrice et second-membre ----------------
	//--------     Ne pas paralleliser  -------------------------
	double *A = malloc(n * n * sizeof(double));
	double *B = malloc(n * n * sizeof(double));
	for (i = 0; i < n; ++i) {
		for (j = 0; j < n; ++j) {
			A[i * n + j] = rand01();
		}
	}
	for (i = 0; i < n; ++i) {
		for (j = 0; j < n; ++j) {
			B[i * n + j] = A[i * n + j] + A[j * n + i];
		}
	}
	for (i = 0; i < n; ++i) {
		for (j = 0; j < n; ++j) {
			A[i * n + j] = B[i * n + j];
		}
		A[i * n + i] = n;
	}
	free(B);
	double *b = malloc(n * sizeof(double));
	for (i = 0; i < n; i++) {
		b[i] = 0.0;
		for (j = 0; j < n; j++)
			b[i] += A[i * n + j];
	}

	/* Prepare variables for the main loop */
	k = 0;

	double *x = malloc(n * sizeof(double));
	double *r = malloc(n * sizeof(double));
	double *p = malloc(n * sizeof(double));
	double *q = malloc(n * sizeof(double));

	/* Initialisation */
	rr = 0.0;
	for (i = 0; i < n; i++) {
		x[i] = 0.0;
		r[i] = b[i];
		p[i] = r[i];
		rr += r[i] * r[i];
	}
	rr0 = rr;
	printf("iter=%d  |g|=%15.8e \n", k, sqrt(rr));

	elapsed = omp_get_wtime();
	while ((k < max_iter) && (rr > tol * tol * rr0)) {
		#pragma omp parallel default(shared) private(i, j)
		{
			#pragma omp single
			k++;	

			/* q=A*p  */
			#pragma omp for
			for (i = 0; i < n; i++) {
				q[i] = 0.0;
				for (j = 0; j < n; j++) {
					q[i] += A[i * n + j] * p[j];
				}
			}

			/* p.q */
			#pragma omp single
			pq = 0.0;
			
			#pragma omp for reduction(+:pq)
			for (i = 0; i < n; i++) {
				pq += p[i] * q[i];
			}

			/* alpha */
			#pragma omp single
			alpha = rr / pq;

			/* Updates */
			#pragma omp for
			for (i = 0; i < n; i++) {
				x[i] = x[i] + alpha * p[i];
				r[i] = r[i] - alpha * q[i];
			}

			/* beta */
			#pragma omp single
			rr1 = 0.0;			

			#pragma omp for reduction(+:rr1)
			for (i = 0; i < n; i++) {
				rr1 += r[i] * r[i];
			}
			
			#pragma omp single
			beta = rr1 / rr;

			/* New p */
			#pragma omp for
			for (i = 0; i < n; i++) {
				p[i] = r[i] + beta * p[i];
			}

			#pragma omp single
			rr = rr1;
		}
		printf("Iter=%d  err=%15.8f \n", k, sqrt(rr / rr0));
	}

	printf("========= Solver Completed ========= \n");
	printf("The code to be timed took %lf seconds\n", omp_get_wtime() - elapsed);
	printf("Number of iterations: %d \n", k);

	/* Free all  allocated memory   */
	free(A);
	free(b);

	free(x);
	free(q);
	free(p);
	free(r);

	return 0;
}